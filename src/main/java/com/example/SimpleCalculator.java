package com.example;

import android.app.Activity;
import com.google.inject.Inject;
import roboguice.inject.ContextSingleton;

@ContextSingleton
public class SimpleCalculator {

    @Inject
    private Activity activity;

    public int add(int x, int y) {
        return x + y;
    }

    public int subtract(int x, int y) {
        return x - y;
    }

}
