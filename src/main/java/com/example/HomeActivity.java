package com.example;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.inject.Inject;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import roboguice.util.RoboAsyncTask;

@ContentView(R.layout.main)
public class HomeActivity extends RoboActivity {

    @InjectView(R.id.toast)
    private Button toaster;

    @InjectView(R.id.addition)
    private Button addition;

    @InjectView(R.id.async)
    private Button async;

    @InjectView(R.id.timer)
    private TextView timerView;

    @Inject
    private Context appContext;

    @Inject
    private SimpleCalculator simpleCalculator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toastSomething("This is a toast br0!");
            }
        });

        addition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toastSomething("4+4=" + simpleCalculator.add(4, 4));
            }
        });

        async.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    new AsyncOperation(HomeActivity.this).execute();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private void toastSomething(String what) {
        Toast.makeText(appContext, what, Toast.LENGTH_LONG).show();
    }

    private class AsyncOperation extends RoboAsyncTask<Integer> {

        private int howManySeconds;

        protected AsyncOperation(Context context) {
            super(context);
            howManySeconds = 5;
        }

        @Override
        public Integer call() throws Exception {
            int millis = howManySeconds;
            for (int i = howManySeconds; i >= 0; i--) {
                Thread.sleep(1000);
//                timerView.setText(Integer.valueOf(i).toString());
            }
            return howManySeconds;
        }

        @Override
        protected void onPreExecute() {
            // do this in the UI thread before executing call()
        }

        @Override
        protected void onSuccess(Integer result) {
            // do this in the UI thread if call() succeeds
            Toast.makeText(HomeActivity.this, "Result: " + result.toString(), Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onException(Exception e) {
            // do this in the UI thread if call() threw an exception
            Toast.makeText(HomeActivity.this, "Ooops. Something went wrong.", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onFinally() {
            // always do this in the UI thread after calling call()
        }

    }

}
